package lempay

import (
	"context"
	"fmt"
	"net/http"

	"gitee.com/ujq/gopay"
)

func (c *Client) doProdPost(ctx context.Context, uri string) (res *http.Response, bs []byte, err error) {

	var url = c.BaseUrl + uri

	req := c.hc.Req() // default json
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	if c.DebugSwitch == gopay.DebugOn {
		c.logger.Debugf("请求URL: %s", url)
		c.logger.Debugf("请求Headers: %#v", req.Header)
	}

	res, bs, err = req.Post(url).EndBytes(ctx)
	if err != nil {
		return nil, nil, err
	}
	if c.DebugSwitch == gopay.DebugOn {
		c.logger.Debugf("Wechat_V3_Response: %d > %s", res.StatusCode, string(bs))
		c.logger.Debugf("Wechat_V3_Rsp_Headers: %#v", res.Header)

	}
	return res, bs, nil
}
func (c *Client) doProdGetApiurl(ctx context.Context, bm gopay.BodyMap) (res *http.Response, bs []byte, err error) {

	// 构建支付 URL
	paymentURL, err := c.RequestParam(bm)
	if err != nil {
		return
	}
	reqUrl := fmt.Sprintf("%s?%s", apiUrl, paymentURL)

	return c.doProdGet(ctx, reqUrl)
}

func (c *Client) doProdGet(ctx context.Context, uri string) (res *http.Response, bs []byte, err error) {
	var url = c.BaseUrl + uri
	req := c.hc.Req() // default json

	if c.DebugSwitch == gopay.DebugOn {
		c.logger.Debugf("请求URL: %s", url)
		c.logger.Debugf("请求Headers: %#v", req.Header)
	}

	res, bs, err = req.Get(url).EndBytes(ctx)
	if err != nil {
		return nil, nil, err
	}

	if c.DebugSwitch == gopay.DebugOn {
		c.logger.Debugf("Wechat_V3_Response: %d > %s", res.StatusCode, string(bs))
		c.logger.Debugf("Wechat_V3_Rsp_Headers: %#v", res.Header)

	}
	return res, bs, nil
}
