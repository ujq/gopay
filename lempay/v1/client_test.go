package lempay

import (
	"context"
	"fmt"
	"html/template"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"testing"

	"gitee.com/ujq/gopay"
	"gitee.com/ujq/gopay/pkg/util"
	"gitee.com/ujq/gopay/pkg/xlog"
)

var (
	ctx    = context.Background()
	client *Client
	err    error

	BaseUrl   = "https://4ab82428.lempay.com/"
	Pid       = 2428                               //商户ID
	SecretKey = "xKXz8P461pf14xfkPQVMKPNk6bKp2pVP" //商户密钥

	isProd = false
)

func TestMain(m *testing.M) {
	client, err = NewClient(Pid, SecretKey, isProd)
	if err != nil {
		xlog.Error(err)
		return
	}
	client.BaseUrl = BaseUrl
	client.NotifyUrl = "http://c.xclapp.com/notify_url.php"
	client.ReturnUrl = "http://c.xclapp.com/return_url.php"

	// 打开Debug开关，输出日志
	client.DebugSwitch = gopay.DebugOn

	os.Exit(m.Run())
}

func TestSubmitGetApi(t *testing.T) {
	tradeNo := util.RandomString(16)

	bm := make(gopay.BodyMap)

	bm.Set("type", "")
	bm.Set("out_trade_no", tradeNo)

	bm.Set("name", "测试订单")
	bm.Set("money", "3")
	bm.Set("param", "")

	rsp, err := client.SubmitApi(ctx, bm)
	if err != nil {
		xlog.Error(err)
		return
	}

	fmt.Println("返回：", rsp)
}

func TestSubmitPostApi(t *testing.T) {

	tradeNo := util.RandomString(16)

	bm := make(gopay.BodyMap)

	bm.Set("type", "")
	bm.Set("out_trade_no", tradeNo)

	bm.Set("name", "测试订单")
	bm.Set("money", "3")
	bm.Set("param", "")

	rsp, err := client.SubmitApi(ctx, bm)
	if err != nil {
		xlog.Error(err)
		return
	}

	fmt.Println("返回：", rsp)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		htmlForm := PagePay(bm)

		// Serve the HTML form
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`
	<!DOCTYPE html>
	<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>正在为您跳转到支付页面，请稍候...</title>
		<style type="text/css">
		body{margin:0;padding:0}
		p{position:absolute;left:50%;top:50%;height:35px;margin:-35px 0 0 -160px;padding:20px;font:bold 16px/30px "宋体",Arial;text-indent:40px;border:1px solid #c5d0dc}
		#waiting{font-family:Arial}
		</style>
	</head>
	<body>
		` + string(htmlForm) + `
		<p>正在为您跳转到支付页面，请稍候...</p>
	</body>
	</html>
	`))

	})

	go func() {
		openBrowser("http://localhost:8080")
	}()

	if err := http.ListenAndServe(":8080", nil); err != nil {
		fmt.Println("ListenAndServe error:", err)
	}

}

func PagePay(bm gopay.BodyMap) template.HTML {
	var html strings.Builder

	submitURL := "https://4ab82428.lempay.com/submit.php" // Replace with the actual submit URL.

	html.WriteString(`<form id="dopay" action="` + submitURL + `" method="post">`)

	for k := range bm {
		html.WriteString(`<input type="hidden" name="` + k + `" value="` + bm.Get(k) + `">`)
	}

	html.WriteString(`<input type="submit" value="正在跳转..."></form><script>document.getElementById("dopay").submit();</script>`)

	return template.HTML(html.String())
}
func openBrowser(url string) error {
	var err error
	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}
	return err
}

// 后端发起支付
func TestMapiApi(t *testing.T) {
	tradeNo := util.RandomString(16)

	bm := make(gopay.BodyMap)
	bm.Set("type", "cashier")
	bm.Set("out_trade_no", tradeNo)

	bm.Set("name", "测试订单")
	bm.Set("money", "3")
	bm.Set("device", "pc")
	bm.Set("param", "")

	rsp, err := client.MapiApi(ctx, bm)
	if err != nil {
		xlog.Error(err)
		return
	}
	fmt.Println("返回：", rsp)

	if rsp.Code == "1" {
		var openUrl string
		if rsp.PayURL != "" {
			openUrl = rsp.PayURL

		} else if rsp.Qrcode != "" {
			openUrl = rsp.Qrcode
		} else if rsp.Urlscheme != "" {
			openUrl = rsp.Urlscheme
		}

		fmt.Println("打开浏览器：", openUrl)

		openBrowser(openUrl)
	}

}

// 查询单个订单
func TestOrderApi(t *testing.T) {
	bm := make(gopay.BodyMap)

	bm.Set("trade_no", "2024081510483727224")
	bm.Set("out_trade_no", "1823914658554580992")
	rsp, err := client.OrderApi(ctx, bm)
	if err != nil {
		xlog.Error(err)
		return
	}
	fmt.Println("返回：", rsp)

}
