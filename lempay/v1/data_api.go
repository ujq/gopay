package lempay

import (
	"context"
	"fmt"
	"net/http"

	"gitee.com/ujq/gopay"
	"gitee.com/ujq/gopay/pkg/gjson"
)

// 页面跳转支付 前端
// https://a913a.lempay.com/submit.php?money=1&name=%E6%B5%8B%E8%AF%95%E8%AE%A2%E5%8D%95&notify_url=http%3A%2F%2Fwww.pay.com%2Fnotify_url.php&out_trade_no=8590505709756467&param=&pid=1058&return_url=http%3A%2F%2Fwww.pay.com%2Fnotify_url.php&sign=0b7e4b64bc185be56463189c58d3da95&sign_type=MD5&type=alipay

func (c *Client) SubmitApi(ctx context.Context, bm gopay.BodyMap) (response string, err error) {

	// 构建支付 URL
	paymentURL, err := c.RequestParam(bm)
	if err != nil {
		return
	}
	response = fmt.Sprintf("%s%s?%s", c.BaseUrl, submitUrl, paymentURL)

	return
}

// API接口支付 后端
func (c *Client) MapiApi(ctx context.Context, bm gopay.BodyMap) (response *PostMapiRsp, err error) {

	bm.Set("clientip", c.ClientIp) //用户发起支付的IP地址
	// 构建支付 URL
	paymentURL, err := c.RequestParam(bm)
	if err != nil {
		return
	}
	reqUrl := fmt.Sprintf("%s?%s", mapiUrl, paymentURL)

	res, bs, err := c.doProdPost(ctx, reqUrl)
	if err != nil {
		return
	}
	if res.StatusCode != http.StatusOK {
		return
	}

	//----解析数据----
	jsonData := gjson.ParseBytes(bs)
	response = &PostMapiRsp{
		Code:      jsonData.Get("code").String(),      //1为成功，其它值为失败
		Msg:       jsonData.Get("msg").String(),       //失败时返回原因
		TradeNo:   jsonData.Get("trade_no").String(),  //支付订单号
		PayURL:    jsonData.Get("payurl").String(),    //如果返回该字段，则直接跳转到该url支付
		Qrcode:    jsonData.Get("qrcode").String(),    //如果返回该字段，则根据该url生成二维码
		Urlscheme: jsonData.Get("urlscheme").String(), //如果返回该字段，则使用js跳转该url，可发起微信小程序支付
	}
	//----解析数据----

	return
}

/*
[API]查询单个订单
URL地址：https://a913a.lempay.com/api.php?act=order&pid={商户ID}&out_trade_no={商户订单号}&sign={32位签名字符串}
*/
func (c *Client) OrderApi(ctx context.Context, bm gopay.BodyMap) (response *OrderQueryRsp, err error) {

	if bm.GetString("trade_no") == "" && bm.GetString("out_trade_no") == "" {
		return nil, fmt.Errorf("%s null", "系统订单号 和 商户订单号 二选一传入即可")
	}
	bm.Set("act", apiActOrder)

	res, bs, err := c.doProdGetApiurl(ctx, bm)
	if err != nil {
		return
	}
	if res.StatusCode != http.StatusOK {
		return
	}

	//----解析数据----
	jsonData := gjson.ParseBytes(bs)
	response = &OrderQueryRsp{
		Code:       jsonData.Get("code").String(),         //1为成功，其它值为失败
		Msg:        jsonData.Get("msg").String(),          //查询订单号成功！
		TradeNo:    jsonData.Get("trade_no").String(),     //柠檬支付订单号
		OutTradeNo: jsonData.Get("out_trade_no").String(), //商户系统内部的订单号
		ApiTradeNo: jsonData.Get("api_trade_no").String(), //支付宝微信等接口方订单号
		Type:       jsonData.Get("type").String(),         //支付方式
		Pid:        jsonData.Get("pid").String(),          //发起支付的商户ID
		AddTime:    jsonData.Get("addtime").String(),      //创建订单时间
		EndTime:    jsonData.Get("endtime").String(),      //完成交易时间
		Name:       jsonData.Get("name").String(),         //商品名称
		Money:      jsonData.Get("money").String(),        //商品金额
		Status:     jsonData.Get("status").String(),       //1为支付成功，0为未支付
		Param:      jsonData.Get("param").String(),        //业务扩展参数
		Buyer:      jsonData.Get("buyer").String(),        //支付者账号
	}
	//----解析数据----
	return
}
