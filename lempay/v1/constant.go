package lempay

/*
接口地址 https://4ab82428.lempay.com/
商户ID：2428
密钥：xKXz8P461pf14xfkPQVMKPNk6bKp2pVP
商户后台：https://api.lempay.com/user/
接口文档：https://api.lempay.com/doc.html

协议规则
URL地址：文档仅供参考，实际以商户后台接口地址为准

请求数据格式：application/x-www-form-urlencode

返回数据格式：JSON

签名算法：MD5

字符编码：UTF-8

*/
const (
	MD5  = "MD5"
	UTF8 = "UTF-8"

	/*
		页面跳转支付
		此接口可用于用户前台直接发起支付，使用form表单跳转或拼接成url跳转。

		URL地址：https://a913a.lempay.com/submit.php

		请求方式：POST 或 GET（推荐POST，不容易被劫持或屏蔽）
	*/
	submitUrl = "submit.php"

	/*
		API接口支付
		此接口可用于服务器后端发起支付请求，会返回支付二维码链接或支付跳转url。

		URL地址：https://a913a.lempay.com/mapi.php

		请求方式：POST
	*/
	mapiUrl = "mapi.php"

	apiUrl = "api.php"
	/*
		[API]查询商户信息
		URL地址：https://a913a.lempay.com/api.php?act=query&pid={商户ID}&sign={32位签名字符串}
	*/
	apiActQuery = "query"
	/*
		[API]查询结算记录
		URL地址：https://a913a.lempay.com/api.php?act=settle&pid={商户ID}&sign={32位签名字符串}
	*/
	apiActSettle = "settle"
	/*
		[API]查询单个订单
		URL地址：https://a913a.lempay.com/api.php?act=order&pid={商户ID}&out_trade_no={商户订单号}&sign={32位签名字符串}
	*/
	apiActOrder = "order"
	/*
		[API]批量查询订单
		URL地址：https://a913a.lempay.com/api.php?act=orders&pid={商户ID}&sign={32位签名字符串}
	*/
	apiActOrders = "orders"
	/*
		[API]提交订单退款
		需要先在商户后台开启订单退款API接口开关，才能调用该接口发起订单退款

		URL地址：https://a913a.lempay.com/api.php?act=refund

		请求方式：POST
	*/
	apiActRefund = "refund"
)
