package lempay

// {"code":1,"trade_no":"2024081509452867419","payurl":"https:\/\/pay.jisufa.com\/pay\/submit\/2024081509452867419\/"}
type PostMapiRsp struct {
	Code      string `json:"code,omitempty"`      // 响应码
	Msg       string `json:"msg,omitempty"`       // 错误信息
	TradeNo   string `json:"trade_no,omitempty"`  // 订单号
	PayURL    string `json:"payurl,omitempty"`    //支付 URL
	Qrcode    string `json:"qrcode,omitempty"`    //如果返回该字段，则根据该url生成二维码
	Urlscheme string `json:"urlscheme,omitempty"` //如果返回该字段，则使用js跳转该url，可发起微信小程序支付
}

/*
{
    "code": 1,
    "msg": "succ",
    "trade_no": "2024081414592697636",
    "out_trade_no": "4539396062296355",
    "api_trade_no": null,
    "type": "alipay",
    "pid": "1058",
    "addtime": "2024-08-14 14:59:26",
    "endtime": null,
    "name": "测试订单",
    "money": "1.00",
    "param": "",
    "buyer": null,
    "status": "0",
    "payurl": null
}
{"code":1,"msg":"succ","trade_no":"2024081616291875812","out_trade_no":"1824362788093825024","api_trade_no":null,"type":false,"pid":"1058","addtime":"2024-08-16
16:29:18","endtime":null,"name":"\u5145\u503c","money":"1.00","param":null,"buyer":null,"status":"0","payurl":null}
*/
// OrderQueryRsp 结构体用于解析查询单个订单的响应
type OrderQueryRsp struct {
	Code       string `json:"code,omitempty"`         // 返回状态码
	Msg        string `json:"msg,omitempty"`          // 返回信息
	TradeNo    string `json:"trade_no,omitempty"`     // 易支付订单号
	OutTradeNo string `json:"out_trade_no,omitempty"` // 商户订单号
	ApiTradeNo string `json:"api_trade_no,omitempty"` // 第三方订单号
	Type       string `json:"type,omitempty"`         // 支付方式
	Pid        string `json:"pid,omitempty"`          // 商户ID
	AddTime    string `json:"addtime,omitempty"`      // 创建订单时间
	EndTime    string `json:"endtime,omitempty"`      // 完成交易时间
	Name       string `json:"name,omitempty"`         // 商品名称
	Money      string `json:"money,omitempty"`        // 商品金额
	Status     string `json:"status,omitempty"`       // 支付状态
	Param      string `json:"param,omitempty"`        // 业务扩展参数
	Buyer      string `json:"buyer,omitempty"`        // 支付者账号
}

/*
pid:1058
trade_no:2024081609265449383
out_trade_no:1824256487309316096
type:alipay
name:product
money:1
trade_status:TRADE_SUCCESS
sign:c10ac8886190e17e12802090e1067f8b
sign_type:MD5
*/
type OrderNotifyRsp struct {
	Pid        string `json:"pid,omitempty"`          // 商户ID
	TradeNo    string `json:"trade_no,omitempty"`     // 易支付订单号
	OutTradeNo string `json:"out_trade_no,omitempty"` // 商户订单号

	Type  string `json:"type,omitempty"`  // 支付方式
	Name  string `json:"name,omitempty"`  // 商品名称
	Money string `json:"money,omitempty"` // 商品金额

	TradeStatus string `json:"trade_status,omitempty"` //只有TRADE_SUCCESS是成功
	Param       string `json:"param,omitempty"`        //业务扩展参数
	Sign        string `json:"sign,omitempty"`         //签名字符串
	SignType    string `json:"sign_type,omitempty"`    //签名类型
}
